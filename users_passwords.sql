-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Мар 15 2018 г., 23:22
-- Версия сервера: 10.1.30-MariaDB
-- Версия PHP: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users_passwords`
--

CREATE TABLE `users_passwords` (
  `users` varchar(50) NOT NULL,
  `hash` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `users_passwords`
--

INSERT INTO `users_passwords` (`users`, `hash`) VALUES
('nikita', '8458b1d651d9faf2691730497b34526730947b758078610ec3a56ebe844fd1a3'),
('vasya', '82a1964fe431dea672a45d068e7cc0ed3737ff26de4adb8afd4558711aacc388'),
('andrei', '876243c75f667f0d0e81db0a1294436498c289ef5065f3cc3a8aac52a2bc4e39'),
('anna', '73064f5d1ada413d9b9b962b1508423e353b4745f30210b7f3280f3ea6360329'),
('katya', '910a19a80a6b09165711d3cf542c4edd88ae12cbe0394dec44ab58bf6db12bd2'),
('nikilai', '9df040eb71132306ca8b32310ab2abab585e1529ada6e752042001e00d030451'),
('adolf', 'cf41b6adf88d1a76ac95bbc9b435848dc7367369748ea5309d15e422265455a5'),
('admin', '5db1fee4b5703808c48078a76768b155b421b210c0761cd6a5d223f4d99f1eaa');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
